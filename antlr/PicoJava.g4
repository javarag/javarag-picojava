grammar PicoJava;

@lexer::header {
package parser;
}

@parser::header {
package parser;
}

options {
	language=Java;
}

program : block EOF;

block:
	LBRACE blockStmt* RBRACE;

blockStmt:
	decl
	| stmt;

decl
locals[String id]:
	CLASS ID (EXTENDS superClassUse=use)? block { 
		$id = $ID.getText();
	} #ClassDecl
	| type=name ID SCOL { $id = $ID.getText(); } # VarDecl;

stmt:
	name ASSIGN exp SCOL # AssignStmt
	| WHILE LPAR exp RPAR stmt # WhileStmt;

exp:
	name              # ExpName
	| TRUE            # BooleanLiteral
	| FALSE           # BooleanLiteral
	;

name: 
	use				#SimpleName
	| name DOT use	#Dot;

use:
	ID;

CLASS: 'class';
EXTENDS: 'extends';
WHILE: 'while';

TRUE: 'true';
FALSE: 'false';

LPAR: '(';
RPAR: ')';
LBRACE: '{';
RBRACE: '}';
SCOL: ';';
DOT: '.';

ASSIGN: '=';

ID: [a-zA-Z][a-zA-Z0-9]*;

//LineTerminator: [(\r)(\n)(\r\n);
//InputCharacter: [^\r\n];

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines
