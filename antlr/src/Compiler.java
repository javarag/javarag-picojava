
import ast.RootContext;
import compiler.PicoJava;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javarag.AttributeEvaluator;

public class Compiler {
	public static void main(String args[]) {
		if (args.length != 1) {
			System.out.println("Error: specify a filename");
			System.exit(1);
		}

		InputStream is = null;
		try {
			is = new FileInputStream(args[0]);
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
			System.exit(1);
		} 

		try {
			RootContext root = PicoJava.parseProgram(is);
			AttributeEvaluator e = PicoJava.create(root);
			
			System.out.println(e.<String> evaluate("prettyPrintProgram", root));
			
			List<String> errors = e.evaluate("errors", root);
			if (!errors.isEmpty()) {
				System.out.println("Errors:");
				for (String err: errors) {
					System.out.println("- " + err);
				}
			}
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
			System.exit(1);
		}
	}
}
