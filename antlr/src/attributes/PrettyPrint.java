package attributes;

import org.antlr.v4.runtime.tree.ParseTree;

import ast.RootContext;
import parser.PicoJavaParser.*;
import javarag.Cached;
import javarag.Module;
import javarag.Synthesized;

public class PrettyPrint extends Module<PrettyPrint.Interface> {
	public interface Interface {
		@Synthesized @Cached String prettyPrintProgram(RootContext root);
		@Synthesized @Cached String prettyPrint(ParseTree node, int tabLevel);
		@Synthesized @Cached String show(ExpContext exp);
		@Synthesized @Cached String show(UseContext name);
		@Synthesized @Cached String show(NameContext name);
	}
	
	public String prettyPrintProgram(RootContext root) {
		return e().prettyPrint(root.program(), 0);
	}
	
	public String prettyPrint(ProgramContext program, int t) {
		return e().prettyPrint(program.block(), t);
	}

	public String prettyPrint(BlockContext block, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		for (BlockStmtContext bs : block.blockStmt()) {
			sb.append(e().prettyPrint(bs, t+1));
		}
		sb.append(ind(t)).append("}\n");
		return sb.toString();
	}

	public String prettyPrint(BlockStmtContext bs, int t) {
 		return e().prettyPrint(bs.getChild(0), t);
	}
 	
	public String prettyPrint(ClassDeclContext cd, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append(ind(t)).append("class ").append(cd.ID());
		if (cd.superClassUse != null) {
			sb.append(" extends ").append(e().show(cd.superClassUse));
		}
		sb.append(" ");
		sb.append(e().prettyPrint(cd.block(), t));
		return sb.toString();
	}
	
 	
	public String prettyPrint(VarDeclContext vd, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append(ind(t)).append(e().show(vd.name())).append(" ");
		sb.append(vd.ID()).append(";\n");
		return sb.toString();
	}
	
	public String prettyPrint(AssignStmtContext asg, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append(ind(t)).append(e().show(asg.name())).append(" = ");
		sb.append(e().show(asg.exp())).append(";\n");	
		return sb.toString();
	}
	
	public String prettyPrint(WhileStmtContext whileStmt, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append(ind(t)).append("while (");
		sb.append(e().show(whileStmt.exp())).append(")\n");
		sb.append(e().prettyPrint(whileStmt.stmt(), t+1));
		return sb.toString();
	}
	
	public String show(ExpNameContext expName) {
		return e().show(expName.name());
	}
	public String show(SimpleNameContext use) {
		return e().show(use.use());
	}
	public String show(UseContext use) {
		return use.ID().getText();
	}
	public String show(DotContext dot) {
		return e().show(dot.name()) + "." + e().show(dot.use());
	}
	public String show(BooleanLiteralContext bolLit) {
		return bolLit.getText();
	}

	private String ind(int tabLevel) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tabLevel; i++) {
			sb.append("\t");
		}
		return sb.toString();
	}
}
