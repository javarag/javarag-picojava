package attributes;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import ast.RootContext;
import parser.PicoJavaParser.*;
import javarag.Cached;
import javarag.Inherited;
import javarag.Module;
import javarag.Procedural;
import javarag.Synthesized;

//@Memoized("errors")
public class ErrorCheck<T extends 
		ErrorCheck.Interface
		& ErrorCheck.LocalInterface
		& NameAnalysis.Interface
		& NullObjects.Interface
		& TypeAnalysis.Interface> extends Module<T> {
	
	public interface Interface {
		@Synthesized @Cached List<String> errors(ProgramContext program);
	}
	
	protected interface LocalInterface {
		@Procedural void collectErrors(ParseTree node, List<String> list);
		@Inherited @Cached NameContext qualifier(UseContext useNode, UseContext useParam);
	}
	
	public List<String> errors(RootContext root) {
		List<String> list = new ArrayList<String>();
		e().collectErrors(root.program(), list);
		return list;
	}

	public void collectErrors(ParserRuleContext node, List<String> list) {
		traverseChildren(node, list);
	}
	public void collectErrors(TerminalNodeImpl node, List<String> list) {
		// do nothing for terminals
		// TODO: change TerminalNodeImpl to TerminalNode (interface)
	}
	
	public void collectErrors(AssignStmtContext asg, List<String> list) {
		traverseChildren(asg, list);
		DeclContext tdValue = e().type(asg.exp());
		DeclContext tdVar = e().type(asg.name());
		if (!e().isSubtypeOf(tdValue, tdVar)) {
			list.add("Can not assign a variable of type " + tdVar.id
					+ " to a value of type " + tdValue.id);
		}
	}
	
	public void collectErrors(ClassDeclContext cd, List<String> list) {
		traverseChildren(cd, list);
		if (e().hasCycleOnSuperclassChain(cd)) 			
			list.add("Cyclic inheritance chain for class " + cd.id);
	}
	
	public void collectErrors(WhileStmtContext stmt, List<String> list) {
		traverseChildren(stmt, list);
		if (!e().isSubtypeOf(e().type(stmt.exp()), e().booleanType(stmt))) 
			list.add("Condition must be a boolean expression");
		if (!e().isValue(stmt.exp())) 
			list.add("Condition must be a value");
	}
	
	public void collectErrors(UseContext use, List<String> list) {
		traverseChildren(use, list);
		if (e().isUnknown(e().decl(use))
				&& (!e().isQualified(use)
					|| !e().isUnknown(e().type(e().qualifier(use, use))))) {
			list.add("Unknown identifier " + use.ID().getText());
		}
	}

	public NameContext qualifier(DotContext dot, UseContext use) {
		return dot.use() == use ? dot.name() : null;
	}
	public NameContext qualifier(ProgramContext p, UseContext use) {
		throw new Error("Can not compute qualifier for non qualified names");
	}	
	
	private void traverseChildren(ParseTree node, List<String> list) {
		for (int i = 0; i < node.getChildCount(); i++) {
			e().collectErrors(node.getChild(i), list);
		}
	}
}
