package attributes;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import ast.RootContext;
import javarag.Cached;
import javarag.Inherited;
import javarag.Module;
import javarag.Synthesized;
import parser.PicoJavaParser.*;

//@Memoized({"decl", "localLookup"})
public class NameAnalysis<T extends 
		NameAnalysis.LocalInterface 
		& NameAnalysis.Interface 
		& TypeAnalysis.Interface
		& NullObjects.Interface> extends Module<T> {
	
	public interface Interface {
		@Synthesized @Cached DeclContext decl(NameContext access);
		@Synthesized @Cached DeclContext decl(UseContext access);
		
		@Inherited @Cached DeclContext lookup(ClassDeclContext cd, String name);
		@Inherited @Cached DeclContext lookup(UseContext use, String name);
		@Inherited @Cached DeclContext lookup(BlockContext block, String name);
		
		@Synthesized @Cached DeclContext remoteLookup(DeclContext td, String name);
		
		@Synthesized @Cached DeclContext localLookup(BlockContext block, String name);
		@Synthesized @Cached DeclContext localLookup(RootContext root, String name);
		
		@Synthesized @Cached DeclContext declarationOf(ParseTree node, String name);
		
		@Synthesized @Cached boolean isQualified(UseContext use);
	}
	
	protected interface LocalInterface {
		@Inherited @Cached boolean isRHSOfDot(UseContext useNode, UseContext useParam);
		@Inherited @Cached DeclContext dotLookup(UseContext use, String name);
	}
	
	public DeclContext decl(SimpleNameContext use) {
		return e().decl(use.use());
	}
	
	public DeclContext decl(UseContext use) {
		if (e().isQualified(use)) {
			return e().dotLookup(use, use.ID().getText());
		} else {
			return e().lookup(use, use.ID().getText());
		}
	}
	
	public DeclContext decl(DotContext dot) {
		return e().decl(dot.use());
	}
	
	public DeclContext lookup(RootContext root, String name) {
		return e().localLookup(root, name);
	}
	
	
	public DeclContext lookup(BlockContext block, String name) {
		DeclContext decl = e().localLookup(block, name);
		if (!e().isUnknown(decl)) return decl;
		return e().lookup(block, name);
	}
	
	
	public DeclContext lookup(ClassDeclContext cd, String name) {
		ClassDeclContext superClass = e().superClass(cd);
		if (superClass != null) {
			DeclContext decl = e().remoteLookup(superClass, name);
			if (!e().isUnknown(decl)) return decl;
		}
		return e().lookup(cd, name);
	}

	
	public DeclContext dotLookup(DotContext dot, String name) {
		DeclContext decl = e().decl(dot.name());
		return e().remoteLookup(e().type(decl), name);
	}
	
	
	public DeclContext remoteLookup(DeclContext td, String name) {
		return e().unknownDecl(td);
	}
	public DeclContext remoteLookup(ClassDeclContext cd, String name) {
		DeclContext decl = e().localLookup(cd.block(), name);
		if (!e().isUnknown(decl)) return decl;
		if (e().superClass(cd) != null) {
			decl = e().remoteLookup(e().superClass(cd), name);
			if (!e().isUnknown(decl)) return decl;
		}
		return e().unknownDecl(cd);
	}
	
	public DeclContext localLookup(BlockContext block, String name) {
		for (BlockStmtContext blockStmt: block.blockStmt()) {
			DeclContext decl = e().declarationOf(blockStmt, name);
			if (decl != null) return decl;
		}
		return e().unknownDecl(block);
	}
	
	public DeclContext localLookup(RootContext root, String name) {
		for (DeclContext d: root.getPredefinedDecls()) {
			DeclContext decl = e().declarationOf(d, name);
			if (decl != null) return decl;
		}
		return e().localUnknownDecl(root);
	}

	public DeclContext declarationOf(ParserRuleContext node, String name) {
		return null;
	}
	public DeclContext declarationOf(BlockStmtContext blockStmt, String name) {
		return e().declarationOf(blockStmt.getChild(0), name);
	}
	public DeclContext declarationOf(DeclContext decl, String name) {
		return decl.id.equals(name) ? decl : null;
	}
	
	
	public boolean isQualified(UseContext use) {
		return e().isRHSOfDot(use, use);
	}
	public boolean isRHSOfDot(DotContext dot, UseContext use) {
		return dot.use() == use;
	}
	public boolean isRHSOfDot(RootContext root, UseContext use) {
		return false;
	}
}
