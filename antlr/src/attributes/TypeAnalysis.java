package attributes;

import org.antlr.v4.runtime.tree.ParseTree;

import ast.PrimitiveDeclContext;
import ast.RootContext;
import ast.UnknownDeclContext;
import javarag.Bottom;
import javarag.Cached;
import javarag.Circular;
import javarag.Inherited;
import javarag.Module;
import javarag.Synthesized;
import parser.PicoJavaParser.*;

//@Memoized({"booleanType", "type", "isSubtypeOf", "hasCycleOnSuperclassChain", "superClass", "isSuperTypeOf", "isSuperTypeOfClassDecl"})
public class TypeAnalysis<T extends 
		TypeAnalysis.LocalInterface
		& TypeAnalysis.Interface 
		& NameAnalysis.Interface> extends Module<T> {

	public interface Interface {
		@Synthesized @Cached DeclContext type(DeclContext decl);
		@Synthesized @Cached DeclContext type(ExpContext exp);
		@Synthesized @Cached DeclContext type(NameContext use);
		@Synthesized @Cached DeclContext type(UseContext use);
		@Synthesized @Cached boolean isSubtypeOf(DeclContext tdNode, DeclContext tdParam);
		@Synthesized @Cached boolean isValue(ExpContext exp);
		@Synthesized @Cached boolean isValue(NameContext exp);
		@Synthesized @Cached boolean isValue(UseContext exp);
		@Inherited @Cached PrimitiveDeclContext booleanType(ParseTree node);
		@Synthesized @Cached PrimitiveDeclContext localBooleanType(RootContext root);
		@Synthesized @Cached ClassDeclContext superClass(ClassDeclContext cd);
		@Circular @Synthesized boolean hasCycleOnSuperclassChain(ClassDeclContext cd);
	}
	
	protected interface LocalInterface {
		@Synthesized @Cached boolean isSuperTypeOf(DeclContext tdNode, DeclContext tdParam);
		@Synthesized @Cached boolean isSuperTypeOfClassDecl(DeclContext td, ClassDeclContext cd);
	}
	
	
	public DeclContext type(DeclContext td) {
		return td;
	}
	public DeclContext type(VarDeclContext vd) {
		return e().type(e().decl(vd.type));
	}
	public DeclContext type(ExpNameContext expName) {
		return e().type(expName.name());
	}
	public DeclContext type(SimpleNameContext name) {
		return e().type(name.use());
	}
	public DeclContext type(DotContext dot) {
		return e().type(dot.use());
	}
	public DeclContext type(UseContext use) {
		return e().type(e().decl(use));
	}
	public DeclContext type(BooleanLiteralContext boolLit) {
		return e().booleanType(boolLit);
	}
	
	public boolean isSubtypeOf(DeclContext tdNode, DeclContext tdParam) {
		return e().isSuperTypeOf(tdParam, tdNode);
	}
	public boolean isSubtypeOf(ClassDeclContext cd, DeclContext td) {
		return e().isSuperTypeOfClassDecl(td, cd);
	}
	public boolean isSuperTypeOf(DeclContext tdNode, DeclContext tdParam) {
		return tdNode == tdParam;
	}
	public boolean isSuperTypeOfClassDecl(DeclContext td, ClassDeclContext cd) {
		if (td == cd) return true;
		ClassDeclContext superClass = e().superClass(cd);
		return superClass != null && e().isSubtypeOf(superClass, td);
	}
	
	public boolean isValue(ExpContext exp) {
		return true;
	}
	public boolean isValue(ExpNameContext expName) {
		return e().isValue(expName.name());
	}

	public boolean isValue(SimpleNameContext name) {
		return e().isValue(name.use());
	}
	public boolean isValue(DotContext dot) {
		return e().isValue(dot.use());
	}
	public boolean isValue(UseContext use) {
		DeclContext decl = e().decl(use);
		// TODO: use dynamic dispatch instead
		return !(decl instanceof ClassDeclContext) 
				&& !(decl instanceof PrimitiveDeclContext)
				&& !(decl instanceof UnknownDeclContext);
	}
	
	public ClassDeclContext superClass(ClassDeclContext cd) {
		if (cd.superClassUse != null) {
			// TODO: TEMPORARY HACK - since equations (lookup) cannot be defined for specific children
			// We want to do: 
			//   Decl decl = e().decl(cd.getSuperClass());
			DeclContext decl = e().lookup(cd, cd.superClassUse.ID().getText());
			if (decl instanceof ClassDeclContext && !e().hasCycleOnSuperclassChain(cd)) {
				return (ClassDeclContext) decl;
			}
		}
		return null;
	}
	
	
	
	@Bottom("hasCycleOnSuperclassChain")
	public boolean hasCycleOnSuperclassChainStart(ClassDeclContext cd) {
		return true;
	}

	public boolean hasCycleOnSuperclassChain(ClassDeclContext cd) {
		if (cd.superClassUse != null) {
			// TODO: HACK!! Se above (superClass)
			// Decl decl = e().decl(cd.getSuperClass());
			DeclContext decl = e().lookup(cd, cd.superClassUse.ID().getText());
			if (decl instanceof ClassDeclContext) {
				return e().hasCycleOnSuperclassChain((ClassDeclContext) decl);
			}
		}
		return false;
	}
	
	public PrimitiveDeclContext booleanType(RootContext root) {
		return e().localBooleanType(root);
	}
	public PrimitiveDeclContext localBooleanType(RootContext root) {
		return (PrimitiveDeclContext) e().localLookup(root, "boolean");
	}
}
