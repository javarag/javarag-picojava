package attributes;

import org.antlr.v4.runtime.tree.ParseTree;

import ast.RootContext;
import ast.UnknownDeclContext;
import parser.PicoJavaParser.*;
import javarag.Cached;
import javarag.Inherited;
import javarag.Module;
import javarag.Synthesized;

//@Memoized({"localUnknownDecl"})
public class NullObjects<T extends NullObjects.Interface & NameAnalysis.Interface> extends Module<T>{
	public interface Interface {
		@Synthesized @Cached boolean isUnknown(DeclContext decl);
		@Inherited @Cached UnknownDeclContext unknownDecl(ParseTree node);
		@Synthesized @Cached UnknownDeclContext localUnknownDecl(RootContext root);
	}
	
	public boolean isUnknown(DeclContext decl) {
		return false;
	}
	public boolean isUnknown(UnknownDeclContext decl) {
		return true;
	}
	
	// all types are compatible with the unknown type
	public boolean isSubtypeOf(UnknownDeclContext ud, DeclContext td) {
		return true;
	}
	public boolean isSuperTypeOf(UnknownDeclContext ud, DeclContext td) {
		return true;
	}
	public boolean isSuperTypeOfClassDecl(UnknownDeclContext ud, ClassDeclContext cd) {
		return true;
	}
	
	public UnknownDeclContext unknownDecl(RootContext root) {
		return e().localUnknownDecl(root);
	}
	public UnknownDeclContext localUnknownDecl(RootContext root) {
		return (UnknownDeclContext) e().localLookup(root, "$unknown");
	}
}
