package compiler;

import java.io.IOException;
import java.io.InputStream;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import parser.PicoJavaLexer;
import parser.PicoJavaParser;
import parser.PicoJavaParser.ProgramContext;
import javarag.AttributeEvaluator;
import javarag.AttributeRegister;
import javarag.impl.reg.BasicAttributeRegister;
import ast.RootContext;
import attributes.ErrorCheck;
import attributes.NameAnalysis;
import attributes.NullObjects;
import attributes.PrettyPrint;
import attributes.TypeAnalysis;

public final class PicoJava {
	private PicoJava() { }
	
	public static RootContext parseProgram(InputStream is) throws IOException {
		ANTLRInputStream input = new ANTLRInputStream(is);
		PicoJavaLexer lexer = new PicoJavaLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		PicoJavaParser parser = new PicoJavaParser(tokens);
		ProgramContext p = parser.program();
		return new RootContext(p);
	}
	
	public static AttributeEvaluator create(RootContext root) {
		AttributeRegister registry = new BasicAttributeRegister();
		registry.register(PrettyPrint.class, NameAnalysis.class, TypeAnalysis.class, NullObjects.class, ErrorCheck.class);
		return registry.getEvaluator(root, new PicoJavaTreeTraverser());
	}
}
