package compiler;
import java.util.ArrayList;

import javarag.TreeTraverser;

import org.antlr.v4.runtime.tree.ParseTree;


public class PicoJavaTreeTraverser implements TreeTraverser<ParseTree> {
	@Override
	public Iterable<? extends ParseTree> getChildren(ParseTree root) {
		ArrayList<ParseTree> list = new ArrayList<ParseTree>();
		for (int i = 0; i < root.getChildCount(); i++) {
			list.add(root.getChild(i));
		}
		return list;
	}
}
