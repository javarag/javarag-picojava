package ast;

import parser.PicoJavaParser.DeclContext;

public class UnknownDeclContext extends DeclContext {
	public UnknownDeclContext() {
		this.id = "$unknown";
	}
}