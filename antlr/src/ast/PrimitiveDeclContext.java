package ast;

import parser.PicoJavaParser.DeclContext;

public class PrimitiveDeclContext extends DeclContext {
	public PrimitiveDeclContext(String id) {
		this.id = id;
	}
}