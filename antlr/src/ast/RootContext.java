package ast;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;

import parser.PicoJavaParser.DeclContext;
import parser.PicoJavaParser.ProgramContext;

public class RootContext extends ParserRuleContext {
	private final ProgramContext program;
	private final List<DeclContext> predefinedDecls;

	public RootContext(ProgramContext program) {
		this.program = program;
		predefinedDecls = Collections.unmodifiableList(
				Arrays.asList(
						new PrimitiveDeclContext("boolean"),
						new UnknownDeclContext()));

		program.parent = this;
		addChild(program);
		for (DeclContext decl: predefinedDecls) {
			decl.parent = this;
			addChild(decl);
		}
	}

	public List<DeclContext> getPredefinedDecls() {
		return predefinedDecls;
	}

	public ProgramContext program() {
		return program;
	}
}
