package tests;

import javarag.AttributeEvaluator;

import org.junit.Test;

import ast.PrimitiveDeclContext;
import ast.RootContext;
import ast.UnknownDeclContext;
import compiler.PicoJava;
import parser.PicoJavaParser.*;
import tests.utils.AbstractTestSuite;
import static org.junit.Assert.*;


public class TestAttributes extends AbstractTestSuite {
	@Test
	public void simpleNameBinding() {
		String s = 
			"{\n" +
			"  boolean a;\n" +
			"  a = true;\n" + 
			"  class A {\n" + 
			"    a = false;\n" +
			"    b = true;\n" + 
			"    class B {\n" +
			"      a = true;\n" + 
			"    }\n"+
			"  }\n" +
			"}";
		RootContext r = parseString(s);
		ProgramContext p = r.program();
		AttributeEvaluator e = PicoJava.create(r);

		VarDeclContext vd = (VarDeclContext) p.block().blockStmt(0).decl();
		AssignStmtContext asg = (AssignStmtContext) p.block().blockStmt(1).stmt();

		assertSame(vd, e.evaluate("decl", asg.name()));
		
		ClassDeclContext cd = (ClassDeclContext) p.block().blockStmt(2).decl();
		AssignStmtContext asg2 = (AssignStmtContext) cd.block().blockStmt(0).stmt();
		AssignStmtContext asg3 = (AssignStmtContext) cd.block().blockStmt(1).stmt();
		
		assertSame(vd, e.evaluate("decl", asg2.name()));
		
		UnknownDeclContext unknown = e.evaluate("localUnknownDecl", r);
		assertNotNull(unknown);
		assertSame(unknown, e.evaluate("decl", asg3.name()));
	}
	
	@Test
	public void nameBindingInheritance() {
		String s = 
			"{\n" +
			"  class A {\n" + 
			"    boolean a;\n" + 
			"  }\n" +
			"  class B extends A {\n" + 
			"    boolean b;\n" + 
			"  }\n" +
			"  class C extends B {\n" +
			"    a = true;\n" +
			"    b = false;\n" +
			"  }\n" +
			"}";
		RootContext r = parseString(s);
		ProgramContext p = r.program();
		AttributeEvaluator e = PicoJava.create(r);

		ClassDeclContext cdA = (ClassDeclContext) p.block().blockStmt(0).decl();
		ClassDeclContext cdB = (ClassDeclContext) p.block().blockStmt(1).decl();
		ClassDeclContext cdC = (ClassDeclContext) p.block().blockStmt(2).decl();

		VarDeclContext vdA = (VarDeclContext) cdA.block().blockStmt(0).decl();
		VarDeclContext vdB = (VarDeclContext) cdB.block().blockStmt(0).decl();

		AssignStmtContext asg1 = (AssignStmtContext) cdC.block().blockStmt(0).stmt();
		AssignStmtContext asg2 = (AssignStmtContext) cdC.block().blockStmt(1).stmt();
		
		assertSame(vdA, e.evaluate("decl", asg1.name()));
		assertSame(vdB, e.evaluate("decl", asg2.name()));
	}
	
	@Test
	public void nameBindingRemote() {
		String s = 
			"{\n" +
			"  class A {\n" + 
			"    boolean bvar1;\n" + 
			"  }\n" +
			"  class B extends A {\n" + 
			"    boolean bvar2;\n" + 
			"  }\n" +
			"  class C {\n" +
			"    B b;\n" + 
			"    b.bvar1 = true;\n" +
			"    b.bvar2 = true;\n" + 
			"    b.bvar3 = false;\n" +
			"  }\n" +
			"}";
		RootContext r = parseString(s);
		ProgramContext p = r.program();
		AttributeEvaluator e = PicoJava.create(r);

		ClassDeclContext cdA = (ClassDeclContext) p.block().blockStmt(0).decl();
		ClassDeclContext cdB = (ClassDeclContext) p.block().blockStmt(1).decl();
		ClassDeclContext cdC = (ClassDeclContext) p.block().blockStmt(2).decl();

		VarDeclContext vdBVar1 = (VarDeclContext) cdA.block().blockStmt(0).decl();
		VarDeclContext vdBVar2 = (VarDeclContext) cdB.block().blockStmt(0).decl();

		AssignStmtContext asg1 = (AssignStmtContext) cdC.block().blockStmt(1).stmt();
		AssignStmtContext asg2 = (AssignStmtContext) cdC.block().blockStmt(2).stmt();
		AssignStmtContext asg3 = (AssignStmtContext) cdC.block().blockStmt(3).stmt();
		
		assertSame(vdBVar1, e.evaluate("decl", asg1.name()));
		assertSame(vdBVar2, e.evaluate("decl", asg2.name()));
		assertSame(e.evaluate("localUnknownDecl", r), e.evaluate("decl", asg3.name()));
	}
	
	@Test
	public void typeBinding() {
		String s = 
			"{\n" +
			"  class A {\n" + 
			"    A a;\n" +
			"    boolean b;\n" + 
			"  }\n" +
			"  class B extends A {\n" + 
			"  }\n" +
			"}";
		RootContext r = parseString(s);
		ProgramContext p = r.program();
		AttributeEvaluator e = PicoJava.create(r);

		ClassDeclContext cdA = (ClassDeclContext) p.block().blockStmt(0).decl();
		VarDeclContext vdA = (VarDeclContext) cdA.block().blockStmt(0).decl();
		VarDeclContext vdB = (VarDeclContext) cdA.block().blockStmt(1).decl();
		assertSame(cdA, e.evaluate("decl", vdA.name()));

		PrimitiveDeclContext booleanDecl = e.evaluate("localBooleanType", r);
		assertNotNull(booleanDecl);
		assertSame(booleanDecl, e.evaluate("decl", vdB.name()));
	
		ClassDeclContext cdB = (ClassDeclContext) p.block().blockStmt(1).decl();
		assertSame(cdA, e.evaluate("decl", cdB.superClassUse));
		assertSame(cdA, e.evaluate("superClass", cdB));
	}
	
	@Test
	public void cyclicInheritance() {
		String s = 
			"{\n" +
			"  class A extends B {\n" + 
			"  }\n" +
			"  class B extends A {\n" + 
			"  }\n" +
			"  class C extends C {\n" + 
			"  }\n" +
			"}";
		RootContext r = parseString(s);
		AttributeEvaluator e = PicoJava.create(r);

		ClassDeclContext cdA = (ClassDeclContext) r.program().block().blockStmt(0).decl();
		ClassDeclContext cdB = (ClassDeclContext) r.program().block().blockStmt(1).decl();
		ClassDeclContext cdC = (ClassDeclContext) r.program().block().blockStmt(2).decl();

		assertNull(e.evaluate("superClass", cdA));
		assertNull(e.evaluate("superClass", cdB));
		assertNull(e.evaluate("superClass", cdC));
	}
	
	@Test
	public void isSubTypeOf() {
		String s = 
		"{\n" +
		"  class A {\n" +
		"  }\n" +
		"  class B extends A {\n" +
		"  }\n" +
		"  class C extends B {\n" +
		"  }\n" +
		"}\n";
		
		RootContext r = parseString(s);
		ProgramContext p = r.program();
		AttributeEvaluator e = PicoJava.create(r);
		
		ClassDeclContext cdA = (ClassDeclContext) p.block().blockStmt(0).decl();
		ClassDeclContext cdB = (ClassDeclContext) p.block().blockStmt(1).decl();
		ClassDeclContext cdC = (ClassDeclContext) p.block().blockStmt(2).decl();

		assertEquals(false, e.evaluate("isSubtypeOf", cdA, cdB));
		assertEquals(false, e.evaluate("isSubtypeOf", cdA, cdC));
		assertEquals(true, e.evaluate("isSubtypeOf", cdB, cdA));
		assertEquals(true, e.evaluate("isSubtypeOf", cdC, cdB));
		assertEquals(true, e.evaluate("isSubtypeOf", cdC, cdA));
	}
}
