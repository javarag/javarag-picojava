package tests.utils;

import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import ast.RootContext;
import compiler.PicoJava;

public abstract class AbstractTestSuite {
	protected RootContext parseFile(String filename) {
		try {
			return parse(new FileInputStream(filename));
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		}
		return null;
	}
	
	protected RootContext parseString(String s) {
		return parse(new ByteArrayInputStream(s.getBytes()));
	}
	
	protected RootContext parse(InputStream is) {
		try {
			return PicoJava.parseProgram(is);
		} catch (IOException e) {
			fail(e.getMessage());
		}
		return null;
	}
}
