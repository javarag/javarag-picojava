package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Program extends ASTNode {
	private final Block block;
	private final List<TypeDecl> predefinedTypes;
	public Program(Block block) {
		this.block = block;
		predefinedTypes = createPredefinedTypes();
	}
	private List<TypeDecl> createPredefinedTypes() {
		return Arrays.asList(
				new PrimitiveDecl("boolean"),
				new UnknownDecl("$unknown"));
	}
	public Block getBlock() {
		return block;
	}
	public List<TypeDecl> getPredefinedTypes() {
		return predefinedTypes;
	}
	public Iterable<? extends ASTNode> getChildren() {
		ArrayList<ASTNode> list = new ArrayList<ASTNode>();
		list.add(block);
		list.addAll(predefinedTypes);
		return list;
	}
}
