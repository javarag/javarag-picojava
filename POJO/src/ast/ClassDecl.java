package ast;

import java.util.ArrayList;

public class ClassDecl extends TypeDecl {
	private final IdUse superClass;
	private final Block body;
	public ClassDecl(String name, IdUse superClass, Block body) {
		super(name);
		this.superClass = superClass;
		this.body = body;
	}
	public boolean hasSuperClass() {
		return superClass != null;
	}
	public IdUse getSuperClass() {
		return superClass;
	}
	public Block getBody() {
		return body;
	}
	public Iterable<? extends ASTNode> getChildren() {
		ArrayList<ASTNode> list = new ArrayList<ASTNode>();
		if (hasSuperClass()) {
			list.add(superClass);
		}
		list.add(body);
		return list;
	}
}
