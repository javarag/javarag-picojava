package ast;

public abstract class Decl extends Stmt {
	private final String name;
	public Decl(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
}
