package ast;

import java.util.Arrays;

public class Dot extends Access {
	private final Access objectReference;
	private final IdUse idUse;
	public Dot(Access objectReference, IdUse idUse) {
		this.objectReference = objectReference;
		this.idUse = idUse;
	}
	public Access getObjectReference() {
		return objectReference;
	}
	public IdUse getIdUse() {
		return idUse;
	}
	public Iterable<? extends ASTNode> getChildren() {
		return Arrays.asList(objectReference, idUse);
	}
}
