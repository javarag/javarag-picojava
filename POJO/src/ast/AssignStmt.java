package ast;

import java.util.Arrays;

public class AssignStmt extends Stmt {
	private final Access variable;
	private final Exp value;
	public AssignStmt(Access variable, Exp value) {
		this.variable = variable;
		this.value = value;
	}
	public Access getVariable() {
		return variable;
	}
	public Exp getValue() {
		return value;
	}
	public Iterable<? extends ASTNode> getChildren() {
		return Arrays.asList(variable, value);
	}
}
