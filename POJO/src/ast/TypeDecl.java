package ast;

public abstract class TypeDecl extends Decl {
	public TypeDecl(String name) {
		super(name);
	}
}
