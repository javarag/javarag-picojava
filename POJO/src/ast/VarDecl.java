package ast;

import java.util.Arrays;

public class VarDecl extends Decl {
	private final Access type;
	public VarDecl(String name, Access type) {
		super(name);
		this.type = type;
	}
	public Access getType() {
		return type;
	}
	public Iterable<? extends ASTNode> getChildren() {
		return Arrays.asList(type);
	}
}
