package ast;

import java.util.Collections;
import java.util.List;

public class Block extends ASTNode {
	private final List<BlockStmt> blockStmts;
	public Block(List<BlockStmt> blockStmts) {
		this.blockStmts = Collections.unmodifiableList(blockStmts);
	}
	public List<BlockStmt> getBlockStmts() {
		return blockStmts;
	}
	public Iterable<BlockStmt> getChildren() {
		return blockStmts;
	}
}
