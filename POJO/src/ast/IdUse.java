package ast;

public abstract class IdUse extends Access {
	private final String name;
	public IdUse(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
}
