package ast;

public class BooleanLiteral extends Exp {
	private final String value;
	public BooleanLiteral(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}
}
