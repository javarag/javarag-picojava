package ast;

import java.util.Arrays;

public class WhileStmt extends Stmt {
	private final Exp condition;
	private final Stmt body;
	public WhileStmt(Exp condition, Stmt body) {
		this.condition = condition;
		this.body = body;
	}
	public Exp getCondition() {
		return condition;
	}
	public Stmt getBody() {
		return body;
	}
	public Iterable<? extends ASTNode> getChildren() {
		return Arrays.asList(condition, body);
	}
}
