package ast;

import java.util.Collections;

public abstract class ASTNode {
	private int startLine;
	private int startColumn;
	private int endLine;
	private int endColumn;
	
	public void setStart(int startLine, int startColumn) {
		this.startLine = startLine;
		this.startColumn = startColumn;
	}
	public void setEnd(int endLine, int endColumn) {
		this.endLine = endLine;
		this.endColumn = endColumn;
	}
	
	public int getStartLine() { return startLine; }
	public int getStartColumn() { return startColumn; }
	public int getEndLine() { return endLine; }
	public int getEndColumn() { return endColumn; }

	public Iterable<? extends ASTNode> getChildren() {
		return Collections.emptyList();
	}
}
