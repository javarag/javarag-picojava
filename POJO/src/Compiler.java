import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import beaver.Parser.Exception;
import compiler.PicoJavaEvaluator;
import javarag.AttributeEvaluator;
import ast.Program;
import parser.PicoJavaParser;
import parser.PicoJavaScanner;

public class Compiler {
	public static void main(String args[]) {
		if (args.length != 1) {
			System.out.println("Error: specify a filename");
			System.exit(1);
		}
		
		Reader reader = null;
		try {
			reader = new FileReader(args[0]);
		} catch (FileNotFoundException e1) {
			System.out.println("Error: " + e1.getMessage());
			System.exit(1);
		}
		PicoJavaParser parser = new PicoJavaParser();
		PicoJavaScanner scanner = new PicoJavaScanner(new BufferedReader(reader));
		Program p = null;
		try {
			p = (Program)parser.parse(scanner);
			reader.close();
		} catch (IOException e1) {
			System.out.println("Error: " + e1.getMessage());
			System.exit(1);
		} catch (Exception e1) {
			System.out.println("Error: " + e1.getMessage());
			System.exit(1);
		} catch (Error e1) {
			System.out.println("Parse error: " + e1.getMessage());
			System.exit(1);
		}

		AttributeEvaluator e = PicoJavaEvaluator.create(p);
		System.out.println(e.<String> evaluate("prettyPrintProgram", p));
		List<String> errors = e.evaluate("errors", p);
		if (!errors.isEmpty()) {
			System.out.println("Errors:");
			for (String error: errors) {
				System.out.println("- " + error);
			}
		}
	}
}
