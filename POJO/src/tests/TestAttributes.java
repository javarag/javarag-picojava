package tests;

import static org.junit.Assert.*;
import javarag.AttributeEvaluator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import tests.utils.AbstractTestSuite;
import compiler.PicoJavaEvaluator;
import ast.*;

@RunWith(JUnit4.class)
public class TestAttributes extends AbstractTestSuite {
	@Test
	public void primitiveNameBinding() {
		String s = 
		"{\n" +
		"  class A {\n" +
		"    boolean a;\n" +
		"    a = true;" +
		"  }\n" +
		"}\n";
		
		Program p = parse(s);
		AttributeEvaluator e = PicoJavaEvaluator.create(p);
		
		ClassDecl cd = (ClassDecl) p.getBlock().getBlockStmts().get(0);
		VarDecl vd = (VarDecl) cd.getBody().getBlockStmts().get(0);
		AssignStmt asg = (AssignStmt) cd.getBody().getBlockStmts().get(1);
		
		assertSame(vd, e.evaluate("decl", asg.getVariable()));
		TypeDecl td = e.evaluate("type", asg.getVariable());
		assertEquals("boolean", td.getName());
	}

	@Test
	public void nameBindingInherited() {
		String s = 
		"{\n" +
		"  class A {\n" +
		"    boolean a;\n" +
		"  }\n" +
		"  class B extends A {\n" +
		"    a = a;" +
		"  }\n" +
		"}\n";
		
		Program p = parse(s);
		AttributeEvaluator e = PicoJavaEvaluator.create(p);
		
		ClassDecl cdA = (ClassDecl) p.getBlock().getBlockStmts().get(0);
		VarDecl vd = (VarDecl) cdA.getBody().getBlockStmts().get(0);

		ClassDecl cdB = (ClassDecl) p.getBlock().getBlockStmts().get(1);
		AssignStmt asg = (AssignStmt) cdB.getBody().getBlockStmts().get(0);

		assertSame(cdA, e.evaluate("superClass", cdB));
		assertSame(vd, e.evaluate("decl", asg.getVariable()));
		assertSame(vd, e.evaluate("decl", asg.getValue()));
	}

	@Test
	public void remoteAccess() {
		String s = 
		"{\n" +
		"  class A {\n" +
		"    boolean bool;\n" +
		"  }\n" +
		"  class B {\n" +
		"    A a;" +
		"    a.bool = true;" +
		"  }\n" +
		"}\n";
		
		Program p = parse(s);
		AttributeEvaluator e = PicoJavaEvaluator.create(p);
		
		ClassDecl cdA = (ClassDecl) p.getBlock().getBlockStmts().get(0);
		VarDecl vdBool = (VarDecl) cdA.getBody().getBlockStmts().get(0);

		ClassDecl cdB = (ClassDecl) p.getBlock().getBlockStmts().get(1);
		VarDecl vdA = (VarDecl) cdB.getBody().getBlockStmts().get(0);
		AssignStmt asg = (AssignStmt) cdB.getBody().getBlockStmts().get(1);

		assertSame(cdA, e.evaluate("decl", vdA.getType()));
		assertSame(cdA, e.evaluate("type", vdA));
		assertSame(vdBool, e.evaluate("decl", asg.getVariable()));
		TypeDecl td = e.evaluate("type", asg.getVariable());
		assertSame("boolean", td.getName());
	}
	
	@Test
	public void isSubTypeOf() {
		String s = 
		"{\n" +
		"  class A {\n" +
		"  }\n" +
		"  class B extends A {\n" +
		"  }\n" +
		"  class C extends B {\n" +
		"  }\n" +
		"}\n";
		
		Program p = parse(s);
		AttributeEvaluator e = PicoJavaEvaluator.create(p);
		
		ClassDecl cdA = (ClassDecl) p.getBlock().getBlockStmts().get(0);
		ClassDecl cdB = (ClassDecl) p.getBlock().getBlockStmts().get(1);
		ClassDecl cdC = (ClassDecl) p.getBlock().getBlockStmts().get(2);

		assertEquals(false, e.evaluate("isSubtypeOf", cdA, cdB));
		assertEquals(false, e.evaluate("isSubtypeOf", cdA, cdC));
		assertEquals(true, e.evaluate("isSubtypeOf", cdB, cdA));
		assertEquals(true, e.evaluate("isSubtypeOf", cdC, cdB));
		assertEquals(true, e.evaluate("isSubtypeOf", cdC, cdA));
	}
	
	@Test
	public void isSubTypeOfPredefinedTypes() {
		String s = 
		"{\n" +
		"  class A {\n" +
		"    boolean a;\n" +
		"  }\n" +
		"}\n";
		
		Program p = parse(s);
		AttributeEvaluator e = PicoJavaEvaluator.create(p);
		
		ClassDecl cd = (ClassDecl) p.getBlock().getBlockStmts().get(0);
		VarDecl vd = (VarDecl) cd.getBody().getBlockStmts().get(0);
		
		TypeDecl booleanType = e.evaluate("localBooleanType", p);
		TypeDecl unknownDecl = e.evaluate("localUnknownDecl", p);
		assertNotNull(booleanType);
		assertNotNull(unknownDecl);
		assertSame(booleanType, e.evaluate("type", vd.getType()));
		assertTrue((boolean) e.evaluate("isSubtypeOf", booleanType, booleanType));
		assertTrue((boolean) e.evaluate("isSubtypeOf", unknownDecl, booleanType));
		assertTrue((boolean) e.evaluate("isSubtypeOf", unknownDecl, cd));
	}
	
	@Test
	public void cyclicInheritance() {
		String s = 
		"{\n" +
		"  class A extends B {\n" +
		"  }\n" +
		"  class B extends A {\n" +
		"  }\n" +
		"  class C extends C {\n" +
		"  }\n" +
		"}\n";

		Program p = parse(s);
		AttributeEvaluator e = PicoJavaEvaluator.create(p);
		
		ClassDecl cdA = (ClassDecl) p.getBlock().getBlockStmts().get(0);
		ClassDecl cdB = (ClassDecl) p.getBlock().getBlockStmts().get(1);
		ClassDecl cdC = (ClassDecl) p.getBlock().getBlockStmts().get(2);
		
		assertTrue((boolean) e.evaluate("hasCycleOnSuperclassChain", cdA));
		assertTrue((boolean) e.evaluate("hasCycleOnSuperclassChain", cdB));
		assertTrue((boolean) e.evaluate("hasCycleOnSuperclassChain", cdC));

		assertNull(e.evaluate("superClass", cdA));
		assertNull(e.evaluate("superClass", cdB));
		assertNull(e.evaluate("superClass", cdC));
	}
}