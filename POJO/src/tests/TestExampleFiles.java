package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javarag.AttributeEvaluator;

import org.junit.Test;

import tests.utils.AbstractTestSuite;
import compiler.PicoJavaEvaluator;
import ast.Program;

public class TestExampleFiles extends AbstractTestSuite{
	@Test
	public void legal() {
		Program p = parseFile("examples/legal.pj");
		AttributeEvaluator e = PicoJavaEvaluator.create(p);
		
		List<String> errors = e.evaluate("errors", p);
		assertEquals(0, errors.size());
	}
	
	@Test
	public void illegal() {
		Program p = parseFile("examples/illegal.pj");
		AttributeEvaluator e = PicoJavaEvaluator.create(p);
		
		List<String> errors = e.evaluate("errors", p);
		assertEquals(6, errors.size());

		List<String> expected = new ArrayList<String>();
		expected.add("Unknown identifier b");
		expected.add("Can not assign a variable of type boolean to a value of type A");
		expected.add("Condition must be a boolean expression");
		expected.add("Cyclic inheritance chain for class A");
		expected.add("Cyclic inheritance chain for class B");
		expected.add("Can not assign a variable of type C to a value of type D");
		assertEquals(expected, errors);
	}
}
