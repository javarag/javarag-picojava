package tests.utils;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import parser.PicoJavaParser;
import parser.PicoJavaScanner;
import ast.Program;
import beaver.Parser.Exception;

public abstract class AbstractTestSuite {
	protected Program parse(String s) {
		return parse(new StringReader(s));
	}
	
	protected Program parseFile(String filename) {
		try {
			return parse(new FileReader(filename));
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		}
		return null;
	}
	
	protected Program parse(Reader reader) {
		PicoJavaParser parser = new PicoJavaParser();
		PicoJavaScanner scanner = new PicoJavaScanner(new BufferedReader(reader));
		try {
			return (Program)parser.parse(scanner);
		} catch (IOException e) {
			fail(e.getMessage());
		} catch (Exception e) {
			fail(e.getMessage());
		} finally {
			try {
				reader.close();
			} catch (IOException e) { }
		}
		return null;
	}
}
