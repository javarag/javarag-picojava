package attributes;

import ast.*;
import javarag.*;

//@Memoized({"localUnknownDecl"})
public class NullObjects<T extends NullObjects.Interface & NameAnalysis.Interface> extends Module<T>{
	public interface Interface {
		@Synthesized @Cached boolean isUnknown(Decl decl);
		@Inherited @Cached UnknownDecl unknownDecl(ASTNode node);
		@Synthesized @Cached UnknownDecl localUnknownDecl(Program program);
	}
	
	public boolean isUnknown(Decl decl) {
		return false;
	}
	public boolean isUnknown(UnknownDecl decl) {
		return true;
	}
	
	// all types are compatible with the unknown type
	public boolean isSubtypeOf(UnknownDecl ud, TypeDecl td) {
		return true;
	}
	public boolean isSuperTypeOf(UnknownDecl ud, TypeDecl td) {
		return true;
	}
	public boolean isSuperTypeOfClassDecl(UnknownDecl ud, ClassDecl cd) {
		return true;
	}
	
	public UnknownDecl unknownDecl(Program program) {
		return e().localUnknownDecl(program);
	}
	public UnknownDecl localUnknownDecl(Program p) {
		return (UnknownDecl) e().localLookup(p, "$unknown");
	}
}
