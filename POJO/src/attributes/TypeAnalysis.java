package attributes;

import javarag.*;
import ast.*;

//@Memoized({"booleanType", "type", "isSubtypeOf", "hasCycleOnSuperclassChain", "superClass", "isSuperTypeOf", "isSuperTypeOfClassDecl"})
public class TypeAnalysis<T extends 
		TypeAnalysis.LocalInterface
		& TypeAnalysis.Interface 
		& NameAnalysis.Interface> extends Module<T> {

	public interface Interface {
		@Synthesized @Cached TypeDecl type(Decl decl);
		@Synthesized @Cached TypeDecl type(Exp exp);
		@Synthesized @Cached boolean isSubtypeOf(TypeDecl tdNode, TypeDecl tdParam);
		@Synthesized @Cached boolean isValue(Exp exp);
		@Inherited @Cached PrimitiveDecl booleanType(ASTNode node);
		@Synthesized @Cached PrimitiveDecl localBooleanType(Program p);
		@Synthesized @Cached ClassDecl superClass(ClassDecl cd);
		@Circular @Synthesized boolean hasCycleOnSuperclassChain(ClassDecl cd);
	}
	
	protected interface LocalInterface {
		@Synthesized @Cached boolean isSuperTypeOf(TypeDecl tdNode, TypeDecl tdParam);
		@Synthesized @Cached boolean isSuperTypeOfClassDecl(TypeDecl td, ClassDecl cd);
	}
	
	public TypeDecl type(TypeDecl td) {
		return td;
	}
	public TypeDecl type(VarDecl vd) {
		return e().type(e().decl(vd.getType()));
	}
	public TypeDecl type(IdUse idUse) {
		return e().type(e().decl(idUse));
	}
	public TypeDecl type(Dot dot) {
		return e().type(dot.getIdUse());
	}
	public TypeDecl type(BooleanLiteral boolLit) {
		return e().booleanType(boolLit);
	}

	public boolean isSubtypeOf(TypeDecl tdNode, TypeDecl tdParam) {
		return e().isSuperTypeOf(tdParam, tdNode);
	}
	public boolean isSubtypeOf(ClassDecl cd, TypeDecl td) {
		return e().isSuperTypeOfClassDecl(td, cd);
	}
	public boolean isSuperTypeOf(TypeDecl tdNode, TypeDecl tdParam) {
		return tdNode == tdParam;
	}
	public boolean isSuperTypeOfClassDecl(TypeDecl td, ClassDecl cd) {
		if (td == cd) return true;
		ClassDecl superClass = e().superClass(cd);
		return superClass != null && e().isSubtypeOf(superClass, td);
	}
	
	public boolean isValue(Exp exp) {
		return true;
	}
	public boolean isValue(Dot dot) {
		return e().isValue(dot.getIdUse());
	}
	// The following equation is solved by rewrite in the JastAdd solution: 
	// eq TypeUse.isValue() = false;
	public boolean isValue(IdUse idUse) {
		return !(e().decl(idUse) instanceof TypeDecl);
	}
	
	public ClassDecl superClass(ClassDecl cd) {
		if (cd.hasSuperClass()) {
			// TODO: TEMPORARY HACK - since equations (lookup) cannot be defined for specific children
			// We want to do: 
			//   Decl decl = e().decl(cd.getSuperClass());
			Decl decl = e().lookup(cd, cd.getSuperClass().getName());
			if (decl instanceof ClassDecl && !e().hasCycleOnSuperclassChain(cd)) {
				return (ClassDecl) decl;
			}
		}
		return null;
	}
	
	
	@Bottom("hasCycleOnSuperclassChain")
	public boolean hasCycleOnSuperclassChainStart(ClassDecl cd) {
		return true;
	}

	public boolean hasCycleOnSuperclassChain(ClassDecl cd) {
		if (cd.hasSuperClass()) {
			// TODO: HACK!! Se above (superClass)
			// Decl decl = e().decl(cd.getSuperClass());
			Decl decl = e().lookup(cd, cd.getSuperClass().getName());
			if (decl instanceof ClassDecl) {
				return e().hasCycleOnSuperclassChain((ClassDecl) decl);
			}
		}
		return false;
	}
	
	public PrimitiveDecl booleanType(Program program) {
		return e().localBooleanType(program);
	}
	public PrimitiveDecl localBooleanType(Program p) {
		return (PrimitiveDecl) e().localLookup(p, "boolean");
	}
}
