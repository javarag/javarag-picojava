package attributes;

import ast.*;
import javarag.*;

public class PrettyPrint extends Module<PrettyPrint.Interface> {
	public interface Interface {
		@Synthesized @Cached String prettyPrintProgram(Program p);
		@Synthesized @Cached String prettyPrint(ASTNode node, int tabLevel);
		@Synthesized @Cached String show(Exp exp);
	}
	
	public String prettyPrintProgram(Program program) {
		return e().prettyPrint(program, 0);
	}
	
	public String prettyPrint(Program program, int t) {
		return e().prettyPrint(program.getBlock(), t);
	}

	public String prettyPrint(Block block, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		for (BlockStmt bs : block.getBlockStmts()) {
			sb.append(e().prettyPrint(bs, t+1));
		}
		sb.append(ind(t)).append("}\n");
		return sb.toString();
	}
	
	public String prettyPrint(ClassDecl cd, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append(ind(t)).append("class ").append(cd.getName());
		if (cd.hasSuperClass()) {
			sb.append(" extends ").append(e().show(cd.getSuperClass()));
		}
		sb.append(" ");
		sb.append(e().prettyPrint(cd.getBody(), t));
		return sb.toString();
	}
	
	public String prettyPrint(VarDecl vd, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append(ind(t)).append(e().show(vd.getType())).append(" ");
		sb.append(vd.getName()).append(";\n");
		return sb.toString();
	}
	public String prettyPrint(AssignStmt asg, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append(ind(t)).append(e().show(asg.getVariable())).append(" = ");
		sb.append(e().show(asg.getValue())).append(";\n");	
		return sb.toString();
	}
	public String prettyPrint(WhileStmt whileStmt, int t) {
		StringBuilder sb = new StringBuilder();
		sb.append(ind(t)).append("while (");
		sb.append(e().show(whileStmt.getCondition())).append(")\n");
		sb.append(e().prettyPrint(whileStmt.getBody(), t+1));
		return sb.toString();
	}
	
	public String show(Use use) {
		return use.getName();
	}
	public String show(Dot dot) {
		return e().show(dot.getObjectReference()) + "." + e().show(dot.getIdUse());
	}
	public String show(BooleanLiteral bolLit) {
		return bolLit.getValue();
	}

	private String ind(int tabLevel) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tabLevel; i++) {
			sb.append("\t");
		}
		return sb.toString();
	}
}
