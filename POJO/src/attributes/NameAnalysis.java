package attributes;

import javarag.*;
import ast.*;

//@Memoized({"decl", "localLookup"})
public class NameAnalysis<T extends 
		NameAnalysis.LocalInterface 
		& NameAnalysis.Interface 
		& TypeAnalysis.Interface
		& NullObjects.Interface> extends Module<T> {
	
	public interface Interface {
		@Synthesized @Cached Decl decl(Access access);

		@Inherited @Cached Decl lookup(ClassDecl cd, String name);
		@Inherited @Cached Decl lookup(IdUse idUse, String name);
		@Inherited @Cached Decl lookup(Block block, String name);
		
		@Synthesized @Cached Decl remoteLookup(TypeDecl td, String name);

		@Synthesized @Cached Decl localLookup(Block block, String name);
		@Synthesized @Cached Decl localLookup(Program program, String name);
		
		@Synthesized @Cached Decl declarationOf(BlockStmt blockStmt, String name);
		
		@Synthesized @Cached boolean isQualified(IdUse idUse);
	}
	
	protected interface LocalInterface {
		@Inherited @Cached boolean isRHSOfDot(IdUse idUseNode, IdUse idUseParam);
		@Inherited @Cached Decl dotLookup(IdUse idUse, String name);
	}
	
	public Decl decl(IdUse idUse) {
		if (e().isQualified(idUse)) {
			return e().dotLookup(idUse, idUse.getName());
		} else {
			return e().lookup(idUse, idUse.getName());
		}
	}
	public Decl decl(Dot dot) {
		return e().decl(dot.getIdUse());
	}
	
	public Decl lookup(Program program, String name) {
		return e().localLookup(program, name);
	}

	public Decl lookup(Block block, String name) {
		Decl decl = e().localLookup(block, name);
		if (!e().isUnknown(decl)) return decl;
		return e().lookup(block, name);
	}
	
	public Decl lookup(ClassDecl cd, String name) {
		ClassDecl superClass = e().superClass(cd);
		if (superClass != null) {
			Decl decl = e().remoteLookup(superClass, name);
			if (!e().isUnknown(decl)) return decl;
		}
		return e().lookup(cd, name);
	}

	public Decl dotLookup(Dot dot, String name) {
		Decl decl = e().decl(dot.getObjectReference());
		return e().remoteLookup(e().type(decl), name);
	}
	
	public Decl remoteLookup(TypeDecl td, String name) {
		return e().unknownDecl(td);
	}
	public Decl remoteLookup(ClassDecl cd, String name) {
		Decl decl = e().localLookup(cd.getBody(), name);
		if (!e().isUnknown(decl)) return decl;
		if (e().superClass(cd) != null) {
			decl = e().remoteLookup(e().superClass(cd), name);
			if (!e().isUnknown(decl)) return decl;
		}
		return e().unknownDecl(cd);
	}
	
	public Decl localLookup(Block block, String name) {
		for (BlockStmt blockStmt: block.getBlockStmts()) {
			Decl decl = e().declarationOf(blockStmt, name);
			if (decl != null) return decl;
		}
		return e().unknownDecl(block);
	}
	
	public Decl localLookup(Program program, String name) {
		for (TypeDecl td: program.getPredefinedTypes()) {
			Decl decl = e().declarationOf(td, name);
			if (decl != null) return decl;
		}
		return e().localUnknownDecl(program);
	}

	public Decl declarationOf(BlockStmt blockStmt, String name) {
		return null;
	}
	public Decl declarationOf(Decl decl, String name) {
		return decl.getName().equals(name) ? decl : null;
	}
	
	public boolean isQualified(IdUse idUse) {
		return e().isRHSOfDot(idUse, idUse);
	}
	
	/** Private attributes */
	public boolean isRHSOfDot(Dot dot, IdUse idUse) {
		return dot.getIdUse() == idUse;
	}
	public boolean isRHSOfDot(Program p, IdUse idUse) {
		return false;
	}
}
