package attributes;

import java.util.*;

import javarag.*;
import ast.*;

//@Memoized("errors")
public class ErrorCheck<T extends 
		ErrorCheck.Interface
		& ErrorCheck.LocalInterface
		& NameAnalysis.Interface
		& NullObjects.Interface
		& TypeAnalysis.Interface> extends Module<T> {

	
	public interface Interface {
		@Synthesized @Cached List<String> errors(Program program);
	}
	
	protected interface LocalInterface {
		@Procedural void collectErrors(ASTNode node, List<String> list);
		@Inherited @Cached Access qualifier(IdUse idUseNode, IdUse idUseParam);
	}
	
	public List<String> errors(Program program) {
		List<String> list = new ArrayList<String>();
		e().collectErrors(program, list);
		return list;
	}

	public void collectErrors(ASTNode node, List<String> list) {
		traverseChildren(node, list);
	}
	
	public void collectErrors(AssignStmt asg, List<String> list) {
		traverseChildren(asg, list);
		TypeDecl tdValue = e().type(asg.getValue());
		TypeDecl tdVar = e().type(asg.getVariable());
		if (!e().isSubtypeOf(tdValue, tdVar)) {
			list.add("Can not assign a variable of type " + tdVar.getName() 
					+ " to a value of type " + tdValue.getName());
		}
	}
	
	public void collectErrors(ClassDecl cd, List<String> list) {
		traverseChildren(cd, list);
		if (e().hasCycleOnSuperclassChain(cd)) 			
			list.add("Cyclic inheritance chain for class " + cd.getName());
	}
	
	public void collectErrors(WhileStmt stmt, List<String> list) {
		traverseChildren(stmt, list);
		if (!e().isSubtypeOf(e().type(stmt.getCondition()), e().booleanType(stmt))) 
			list.add("Condition must be a boolean expression");
		if (!e().isValue(stmt.getCondition())) 
			list.add("Condition must be a value");
	}
	
	public void collectErrors(IdUse id, List<String> list) {
		traverseChildren(id, list);
		if (e().isUnknown(e().decl(id))
				&& (!e().isQualified(id)
					|| !e().isUnknown(e().type(e().qualifier(id, id))))) {
			list.add("Unknown identifier " + id.getName());
		}
	}

	public Access qualifier(Dot dot, IdUse idUse) {
		return dot.getIdUse() == idUse ? dot.getObjectReference() : null;
	}

	public Access qualifier(Program p, IdUse idUse) {
		throw new Error("Can not compute qualifier for non qualified names");
	}	
	
	private void traverseChildren(ASTNode node, List<String> list) {
		for (ASTNode child: node.getChildren()) {
			e().collectErrors(child, list);
		}
	}
}
