package compiler;

import compiler.PicoJavaTreeTraverser;
import javarag.AttributeEvaluator;
import javarag.AttributeRegister;
import javarag.impl.reg.BasicAttributeRegister;
import ast.Program;
import attributes.ErrorCheck;
import attributes.NameAnalysis;
import attributes.NullObjects;
import attributes.PrettyPrint;
import attributes.TypeAnalysis;

public final class PicoJavaEvaluator {
	public static AttributeEvaluator create(Program p) {
		AttributeRegister registry = new BasicAttributeRegister();
		registry.register(PrettyPrint.class, NameAnalysis.class, TypeAnalysis.class, NullObjects.class, ErrorCheck.class);
		AttributeEvaluator evaluator = registry.getEvaluator(p, new PicoJavaTreeTraverser());
		return evaluator;
	}
}
