package compiler;

import javarag.TreeTraverser;
import ast.ASTNode;


public class PicoJavaTreeTraverser implements TreeTraverser<ASTNode> {
	@Override
	public Iterable<? extends ASTNode> getChildren(ASTNode root) {
//		System.out.println("root of " + root + ":  " + root.getChildren());
		return root.getChildren();
	}
}
