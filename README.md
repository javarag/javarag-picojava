## PicoJava implementations in JavaRAG ##

This project consists of two implementations of PicoJava.

* **POJO**: uses the parser generator Beaver and represents syntax trees using Plain Old Java Objects (POJO)
* **ANTLR**: uses the parser generator ANTLR and its generated syntax tree

PicoJava is a simple object-oriented language that support classes, inheritance, simple statements and fields (see http://jastadd.org)

### Running ###

To run the POJO implementation

	$ cd POJO
	$ ant jar
	$ java -jar picojava-javarag-pojo.jar examples/test.pj 

To run the ANTLR implementation

	$ cd antlr
	$ ant jar
	$ java -jar picojava-javarag-antlr.jar examples/test.pj 
	
The both implementations have test cases. To run these

	$ cd <implementation>
	$ ant test